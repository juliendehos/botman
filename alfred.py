#!/usr/bin/env python3

def genmove(player, board):
    for i, c in enumerate(board):
        if c == '.':
            return i
    return -1

def main():
    while True:
        query = input()
        if query == 'quit':
            break
        [player, board] = query.split(' ')
        move = genmove(player, board)
        print(move)

if __name__ == "__main__":
    main()

