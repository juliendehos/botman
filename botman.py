#!/usr/bin/env python2

import os
import re
import signal
import subprocess
import sys

class Connection:
    def __init__(self, program):
        # open a new process
        self.process = subprocess.Popen(program, stdin=subprocess.PIPE, stdout=subprocess.PIPE, close_fds=True)
        # Connection inputs come from process output
        self.inStream = self.process.stdout
        # Connection outputs go to process input
        self.outStream = self.process.stdin

    # send a command to the process
    def executeCommand(self, command):
        # send command to the connected process
        self.outStream.write(command + "\n")
        self.outStream.flush()

    # send a command to the process and get response 
    def executeCommandResponse(self, command):
        # send command to the connected process
        self.outStream.write(command + "\n")
        self.outStream.flush()
        # get response
        result = self.inStream.readline()
        return result

def updateBoard(player, board, move):
    boardList = list(board)
    boardList[move] = player
    return ''.join(boardList)

def checkWinner(board):
    [b00, b01, b02, b10, b11, b12, b20, b21, b22] = board
    # horizontals
    if b00 != '.' and b00 == b01 == b02:
        return b00
    if b10 != '.' and b10 == b11 == b12:
        return b10
    if b20 != '.' and b20 == b21 == b22:
        return b20
    # verticals
    if b00 != '.' and b00 == b10 == b20:
        return b00
    if b01 != '.' and b01 == b11 == b21:
        return b01
    if b02 != '.' and b02 == b12 == b22:
        return b02
    # diagonals
    if b00 != '.' and b00 == b11 == b22:
        return b00
    if b02 != '.' and b02 == b11 == b20:
        return b02
    return ' '

class Game:
    def __init__(self, circleProg, crossProg):
        self.circleConn = Connection(circleProg) 
        self.crossConn = Connection(crossProg) 

    def quit(self):
        self.circleConn.executeCommand('quit')
        self.crossConn.executeCommand('quit')

    def play(self):
        # init new game
        board = "........."
        currentPlayer = 'O'
        nextPlayer = 'X'
        currentConn = self.circleConn
        nextConn = self.crossConn
        # play the game
        while '.' in board:
            # ask the current player for a move
            query = currentPlayer + ' ' + board
            moveStr = currentConn.executeCommandResponse(query)
            move = int(moveStr)
            # check the move
            if board[move] != '.':
                print 'invalid move:', move, currentPlayer
                return nextPlayer, board
            # check winner
            board = updateBoard(currentPlayer, board, move)
            winner = checkWinner(board)
            if winner != ' ':
                return winner, board
            # next turn
            currentConn, nextConn = nextConn, currentConn
            currentPlayer, nextPlayer = nextPlayer, currentPlayer
        # draw (no move left and no winner)
        return ' ', board

def main():
    # check command line arguments
    if len(sys.argv) != 4:
        print "usage:", sys.argv[0], '<circle prog> <cross prog> <nb games>'
        print "example:", sys.argv[0], './alfred.py ./robin.py 3'
        sys.exit(-1)
    # get command line arguments
    circleProg = sys.argv[1]
    crossProg = sys.argv[2]
    nbGames = int(sys.argv[3])
    # create game
    game = Game(circleProg, crossProg)
    # exit "more nicely" when SIGINT (C-c)
    signalHandler = lambda signal, frame: game.quit()
    signal.signal(signal.SIGINT, signalHandler)
    # run games
    nbCircleWins = 0
    nbCrossWins = 0
    nbDraws = 0
    for i in range(nbGames):
        # play game
        resPlayer, resBoard = game.play()
        print resPlayer, resBoard
        # get final state
        if resPlayer == 'O':
            nbCircleWins = nbCircleWins + 1
        elif resPlayer == 'X':
            nbCrossWins = nbCrossWins + 1
        else:
            nbDraws = nbDraws + 1
    game.quit()
    print "nbGames nbCircleWins nbCrossWins nbDraws"
    print i+1, nbCircleWins, nbCrossWins, nbDraws

if __name__ == "__main__":
    main()

