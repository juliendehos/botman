#!/usr/bin/env python3

import random

def genmove(player, board):
    freeMoves = [i for (i,p) in enumerate(board) if p=='.']
    if not freeMoves:
        return -1
    else:
        return random.choice(freeMoves)

def main():
    while True:
        query = input()
        if query == 'quit':
            break
        [player, board] = query.split(' ')
        move = genmove(player, board)
        print(move)

if __name__ == "__main__":
    main()

