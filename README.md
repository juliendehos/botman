# botman

bot manager for tictactoe

## programs

- `alfred.py` : a bot which chooses the first empty cell
- `robin.py` : a bot which chooses an empty cell randomly
- `botman.py` : the bot manager (with game engine)

```
$ ./botman.py 
usage: ./botman.py <circle prog> <cross prog> <nb games>
example: ./botman.py ./alfred.py ./robin.py 3

$ ./botman.py ./alfred.py ./robin.py 3
X OOXOX.X..
O OOOX....X
O OOO..X..X
nbGames nbCircleWins nbCrossWins nbDraws
3 2 1 0
```

## bot interface

### ask the bot for a move

- botman sends : `<player> <board>`
- bot replies : `<index of an empty cell>`

example:

```
X ...XO..O
1
```

### ask the bot to quit

- botman sends : `quit`

